<?

$ar = array(
    'https://www.advantour.com/rus/russia/exhibitions.htm',
);
function ParserElement($id)
{
    $arrContextOptions = array("ssl" => array("verify_peer" => false, "verify_peer_name" => false));
    $html              = file_get_contents($id, false, stream_context_create($arrContextOptions));
    $temp_data         = [];
    
    //Вырезаем имя
    $preg              = preg_match_all('/<article><h1>(.*?)<\/h1><\/article>/si', $html, $res);
    $temp_data['h1']   = trim($res[1][0]);
    
    //Вырезаем детально дату, город, краткое описание, место проведения, сайт
    $preg = preg_match_all("/<article class='news-img'>(.*?)<\/article>/si", $html, $res1);
    $res2 = $res1[1][0];
    $preg3 = preg_match_all("/<b>Дата<\/b>(.*?)<br(.*?)>/si", $res2, $res3);
    $temp_data['date']   = trim(str_replace(": ", "",$res3[1][0]));
    $preg4 = preg_match_all("/<b>Город<\/b>(.*?)<a href='(.*?)'>(.*?)<\/a>/si", $res2, $res3);
    $temp_data['city']   = trim($res3[3][0]);
    $preg4 = preg_match_all("/<\/a>(.*?)<br(.*?)>(.*?)<div style='clear:both;'>/si", $res2, $res3);
    $temp_data['short_description']   = trim($res3[3][0]);
    $preg4 = preg_match_all("/<p><strong>Место проведения:<\/strong>(.*?)<br(.*?)>/si", $res2, $res3);
    $temp_data['place']   = trim(str_replace("&quot;","",$res3[1][0]));
    $preg4 = preg_match_all("/<strong>Сайт организатора<\/strong>:(.*?)<\/p><h3>/si", $res2, $res3);
    $temp_data['site']   = trim($res3[1][0]);
    $preg4 = preg_match_all("/<p><img src='(.*?)' align='left'>/si", $res2, $res3);
    $temp_data['img']   = trim("https://www.advantour.com".$res3[1][0]);
    
    
    $preg = preg_match_all("/<article class='news-img'>(.*?)<p><strong>(.*?)align='left'>(.*?)<\/article>(.*?)<\/section>/si", $html, $res4);
    $res = trim(strip_tags(html_entity_decode($res4[3][0])));
    $temp_data['text'] = $res;
    return $temp_data;
}

/*function ParserSubCat($id,$page=1){
    $arrContextOptions = array("ssl" => array("verify_peer" => false,"verify_peer_name" => false));
    $html = file_get_contents($id, false, stream_context_create($arrContextOptions));
    $temp_data = [];
    $preg = preg_match_all('/<h1>(.*?)<\/h1>/si', $html, $res);
    $temp_data['h1'] = $res[1][0];
    $preg = preg_match_all('/catalog-item_img(.*?)<div class="catalog-item_title"><a href="(.*?)"/si', $html, $res);
    $temp_data['childs'] = $res[2];
    if(!empty($temp_data['childs']))
        foreach ($temp_data['childs'] as $k => $v)
            $temp_data['childs'][$k] = 'https://www.directelectric.ru'.$v;
    $preg = preg_match_all('/<div class="pagination(.*?)<\/div>/si', $html, $res);
    $maxpage = 1;
    if(!empty($res[1][0])) {
        $preg = preg_match_all('/<span>(.*?)<\/span>/si', $res[1][0], $res);
        if(!empty($res[1]))
            $maxpage = $res[1][count($res[1])-1];
    }
    if($page<$maxpage) {
        $new_temp = ParserSubCat($id,($page+1));
        if(!empty($new_temp['childs']))
            $temp_data['childs'] = array_merge($temp_data['childs'],$new_temp['childs']);
    }
    foreach ($temp_data['childs'] as $k => $v)
        $temp_data['childs'][$k] = ParserElement($temp_data['childs'][$k]);
    return $temp_data;
}*/
function ParserCat($id)
{
    $arrContextOptions = array("ssl" => array("verify_peer" => false, "verify_peer_name" => false));
    $html              = file_get_contents($id, false, stream_context_create($arrContextOptions));
    $temp_data         = [];
    $preg              = preg_match_all("/<a itemprop='url' href='(.*?)'><b><span itemprop='(.*?)'>/si", $html, $res);
    foreach ($res[1] as $url) {
        $temp_data[] = "https://www.advantour.com".$url;
    }
    foreach ($temp_data as $k => $v) {
        $temp_data[$k] = ParserElement($temp_data[$k]);
        die();
    }
    
    return $temp_data;
}

foreach ($ar as $i => $b) {
    file_put_contents($i.'data.json', json_encode(ParserCat($b)));
}
?>