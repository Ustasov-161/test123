<?

$ar = array(
    'https://sales.alfa-eng.ru/fitingi',
);

function ParserSubSubCategory($id)
{
    $arrContextOptions = array(
        "ssl" => array(
            "verify_peer" => false,
            "verify_peer_name" => false,
        ),
    );

    $html = file_get_contents($id, false, stream_context_create($arrContextOptions));
    $temp_data = [];
    $preg = preg_match_all('/<h1>(.*?)<\/h1>/si', $html, $res);
    $temp_data['name'] = trim($res[1][0]);
    $preg = preg_match_all('/<td(.*?)role=cell class="name cart-name">(.*?)<a(.*?)class="table__link verysmall" href=(.*?)>(.*?)<\/a><\/td>/si',
        $html, $res);

    foreach ($res[4] as $k => $v) {
        $temp_data['products'][$k] = ParserElement($v);
    }

    return $temp_data;
}

function ParserElement($id)
{
    $arrContextOptions = array("ssl" => array("verify_peer" => false, "verify_peer_name" => false));
    $html = file_get_contents($id, false, stream_context_create($arrContextOptions));
    $temp_data = [];
    // H1
    $preg = preg_match_all('/<h1 class="catalog__title h1">(.*?)<\/h1>/si', $html, $res);
    $temp_data['name'] = $res[1][0];
    // картинка
    $preg = preg_match_all('/<img(.*?)data-srcset="(.*?)" data-src=(.*?)src="(.*?)" class="attachment-shop-image size-shop-image wp-post-image lazyload"/si',
        $html, $res2);
    $temp_data['img'] = $res2[3][0];
    // цена
    $preg = preg_match_all('/<div(.*?)class="table__text b verysmall">(.*?)<span(.*?)class=text-nowrap>(.*?)<\/span><\/div>/si',
        $html, $res3);
    $preg = preg_match_all('/class="single__price h1">(.*?) ₽<\/div>/si',
        $res3[0][0], $res4);
    $temp_data['price'] = trim(strip_tags($res4[1][0]));
    // свойства
    $preg = preg_match_all('/<table(.*?)class="single__dt dt"><tbody>(.*?)<\/tbody><\/table>/si',
        $html, $res5);
    $preg = preg_match_all('/<div(.*?)class="dt__text h6 b is-dark">(.*?)<\/div>(.*?)<div(.*?)class="dt__text h6 is-dark">(.*?)<\/div>(.*?)/si',
        $res5[0][0], $res6);
    foreach ($res6[2] as $key1 => $value1) {
        $temp_data['props'][trim($value1)] = trim($res6[5][$key1]);
    }
    // артикул
    $preg = preg_match_all('/single__info p"><b(.*?)class=b>(.*?)<\/b>(.*?)<\/div>/si', $html, $res1);
    $temp_data['props']['articul'] = $res1[3][0];
    // title
    $preg = preg_match_all('/<title>(.*?)<\/title>/si',
        $html, $res7);
    $temp_data['title'] = $res7[1][0];
    // description
    $preg = preg_match_all('/<meta(.*?)name=description(.*?)content="(.*?)">/si',
        $html, $res8);
    $temp_data['description'] = $res8[3][0];


    return $temp_data;
}

function ParserSubCat($id, $page = 1)
{
    $arrContextOptions = array("ssl" => array("verify_peer" => false, "verify_peer_name" => false));
    $html = file_get_contents($id, false, stream_context_create($arrContextOptions));
    $temp_data = [];
    $preg = preg_match_all('/<h1>(.*?)<\/h1>/si', $html, $res);
    $temp_data['name'] = trim($res[1][0]);

    $preg = preg_match_all('/<a(.*?)class="single__info-link blue-link" href=(.*?)>(.*?)<\/a>/si', $html,
        $res);

    $temp_data['childs'] = $res[2];

    if (!empty($temp_data['childs'])) {
        foreach ($temp_data['childs'] as $k => $v) {
            $temp_data['childs'][$k] = ParserSubSubCategory($temp_data['childs'][$k]);
        }
    }

    return $temp_data;
}

function ParserCat($id)
{
    $arrContextOptions = array("ssl" => array("verify_peer" => false, "verify_peer_name" => false));
    $html = file_get_contents($id, false, stream_context_create($arrContextOptions));
    $temp_data = [];
    $preg = preg_match_all('/<h1>(.*?)<\/h1>/si', $html, $res);
    $temp_data['name'] = trim($res[1][0]);
    $preg = preg_match_all('/class="single__info-link blue-link" href=(.*?)>(.*?)<\/a>/si', $html, $res1);
    if (!empty($res1[1])) {
        foreach ($res1[2] as $id => $result) {
            $temp_data['childs'][] = ParserSubCat($res1[1][$id]);
        }
    }

    return $temp_data;
}

$data = [];
foreach ($ar as $i => $b) {
    $data[] = ParserCat($b);
}
file_put_contents('/json/data.json', json_encode($data));

?>